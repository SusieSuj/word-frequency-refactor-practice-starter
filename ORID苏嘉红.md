# O

- The topic of today's study is: refactor.
- In the morning, we had code review. My code was shown in class. Actually there are many problems in my code. My team helped me to polish the code and give me a chance to show in class and get more suggestions from the class and teachers. And I was aware of a lot of issues that were not discovered in previous code reviews. I summarized all the issues and try to avoid them later.
- We also had a PPT shown about what is observer mode.
- In the afternoon, we learned refactor in more detail.

# R

- I am very grateful to my team members and teachers for their help. I really like the process of refactoring the code.

# I

- Because when I used to write code by myself, I generally only needed to implement the function, so I didn't know much about many normative things. Code Review taught me a lot about refactoring code.

# D

- I summarized all the issues in these days and try to avoid them later. 