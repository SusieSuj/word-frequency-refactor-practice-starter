import java.util.*;

import java.util.stream.Collectors;

public class WordFrequencyGame {
    public static final String CALCULATE_ERROR = "Calculate Error";
    public static final String S = "\\s+";

    public String countWordFrequency(String words) {
        String[] wordArray = splitStringToArray(words);
        if (wordArray.length == 1) {
            return words + " 1";
        } else {
            try {
                List<Word> wordList = avertStringArrayToList(wordArray);
                Map<String, List<Word>> wordAmountMap = avertListToMap(wordList);
                wordList = computeWordFrequencyAndSort(wordAmountMap);
                return generateWordAndFrequency(wordList);
            } catch (Exception exception) {
                return CALCULATE_ERROR;
            }
        }
    }

    private Map<String, List<Word>> avertListToMap(List<Word> wordList) {
        return wordList.stream().collect(Collectors.groupingBy(Word::getValue));
    }

    private List<Word> avertStringArrayToList(String[] words) {
        return Arrays.stream(words)
                .map(word -> new Word(word, 1))
                .collect(Collectors.toList());
    }

    private List<Word> computeWordFrequencyAndSort(Map<String, List<Word>> wordAmountMap) {
        return wordAmountMap.entrySet()
                .stream()
                .map(word -> new Word(word.getKey(), word.getValue().size()))
                .sorted((word1, word2) -> word2.getWordCount() - word1.getWordCount())
                .collect(Collectors.toList());
    }

    private String generateWordAndFrequency(List<Word> wordList) {
        return wordList.stream()
                .map(word -> word.getValue() + " " + word.getWordCount())
                .collect(Collectors.joining("\n"));
    }

    private String[] splitStringToArray(String words) {
        return words.split(S);
    }
}
